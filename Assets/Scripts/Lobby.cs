using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class Lobby : MonoBehaviourPunCallbacks
{
    [SerializeField] private InputField _createRoomInput;
    [SerializeField] private InputField _joinInput;
    [SerializeField] private InputField _nicknameInput;

    private void Start()
    {
        _nicknameInput.text = PlayerPrefs.GetString("nick");
        PhotonNetwork.NickName = _nicknameInput.text;
    }

    public void CreateRoom()
    {
        RoomOptions roomOptions = new RoomOptions
        {
            MaxPlayers = 4
        };
        Multiplayer._isMultiplayer = true;
        PhotonNetwork.CreateRoom(_createRoomInput.text, roomOptions);
    }

    public void JoinRoom()
    {
        Multiplayer._isMultiplayer = true;
        PhotonNetwork.JoinRoom(_joinInput.text);
    }

    public override void OnJoinedRoom()
    {
        PhotonNetwork.LoadLevel(2);
    }

    public void SaveNickName()
    {
        PlayerPrefs.SetString("nick", _nicknameInput.text);
        PhotonNetwork.NickName = _nicknameInput.text;
    }
}
