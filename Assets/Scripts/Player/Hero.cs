using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public class Hero : MonoBehaviour
{
    [SerializeField] private float _jumpForce = 400f;
    [Range(0, .3f)] [SerializeField] private float _movementSmoothing = .05f;
    [SerializeField] private bool _airControl = false;
    [SerializeField] private LayerMask _whatIsGround;
    [SerializeField] private Transform _groundCheck;
    [SerializeField] private Text TextName;

    const float _groundedRadius = .2f;
    private bool _grounded;
    private Rigidbody2D _rigidbody2D;
    private bool _facingRight = true;
    private Vector3 velocity = Vector3.zero;
    private float _limitFallSpeed = 25f;

    public bool CanDoubleJump = true;
    [SerializeField] private float _dashForce = 25f;
    private bool _canDash = true;
    private bool _isDashing = false;
    private bool _isWall = false;
    private float _prevVelocityX = 0f;

    public float Life = 10f;
    public bool Invincible = false;
    private bool _canMove = true;

    private Animator _animator;
    public ParticleSystem ParticleJumpUp;
    public ParticleSystem ParticleJumpDown;

    private float _jumpWallStartX = 0;
    private float _jumpWallDistX = 0;
    private bool _limitVelOnWallJump = false;

    [Header("Events")]
    [Space]

    public UnityEvent OnFallEvent;
    public UnityEvent OnLandEvent;

    [System.Serializable]
    public class BoolEvent : UnityEvent<bool> { }

    private void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();

        if (OnFallEvent == null)
            OnFallEvent = new UnityEvent();

        if (OnLandEvent == null)
            OnLandEvent = new UnityEvent();
    }

    private void FixedUpdate()
    {
        bool wasGrounded = _grounded;
        _grounded = false;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(_groundCheck.position, _groundedRadius, _whatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
                _grounded = true;
            if (!wasGrounded)
            {
                OnLandEvent.Invoke();
                if (!_isWall && !_isDashing)
                    ParticleJumpDown.Play();
                CanDoubleJump = true;
                if (_rigidbody2D.velocity.y < 0f)
                    _limitVelOnWallJump = false;
            }
        }

        _isWall = false;

        if (!_grounded)
        {
            OnFallEvent.Invoke();
            _prevVelocityX = _rigidbody2D.velocity.x;
        }

        if (_limitVelOnWallJump)
        {
            if (_rigidbody2D.velocity.y < -0.5f)
                _limitVelOnWallJump = false;
            _jumpWallDistX = (_jumpWallStartX - transform.position.x) * transform.localScale.x;
            if (_jumpWallDistX < -0.5f && _jumpWallDistX > -1f)
            {
                _canMove = true;
            }
            else if (_jumpWallDistX < -1f && _jumpWallDistX >= -2f)
            {
                _canMove = true;
                _rigidbody2D.velocity = new Vector2(10f * transform.localScale.x, _rigidbody2D.velocity.y);
            }
            else if (_jumpWallDistX < -2f)
            {
                _limitVelOnWallJump = false;
                _rigidbody2D.velocity = new Vector2(0, _rigidbody2D.velocity.y);
            }
            else if (_jumpWallDistX > 0)
            {
                _limitVelOnWallJump = false;
                _rigidbody2D.velocity = new Vector2(0, _rigidbody2D.velocity.y);
            }
        }
    }


    public void Move(float move, bool jump, bool dash)
    {
        if (_canMove)
        {
            if (dash && _canDash)
            {
                StartCoroutine(DashCooldown());
            }
            if (_isDashing)
            {
                _rigidbody2D.velocity = new Vector2(transform.localScale.x * _dashForce, 0);
            }
            if (_grounded || _airControl)
            {
                if (_rigidbody2D.velocity.y < -_limitFallSpeed)
                    _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, -_limitFallSpeed);
                Vector3 targetVelocity = new Vector2(move * 10f, _rigidbody2D.velocity.y);
                _rigidbody2D.velocity = Vector3.SmoothDamp(_rigidbody2D.velocity, targetVelocity, ref velocity, _movementSmoothing);

                if (move > 0 && !_facingRight)
                {
                    Flip();
                }
                else if (move < 0 && _facingRight)
                {
                    Flip();
                }
            }
            if (_grounded && jump)
            {
                _animator.SetBool("IsJumping", true);
                _animator.SetBool("JumpUp", true);
                _grounded = false;
                _rigidbody2D.AddForce(new Vector2(0f, _jumpForce));
                CanDoubleJump = true;
                ParticleJumpDown.Play();
                ParticleJumpUp.Play();
            }
            else if (!_grounded && jump && CanDoubleJump)
            {
                CanDoubleJump = false;
                _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, 0);
                _rigidbody2D.AddForce(new Vector2(0f, _jumpForce / 1.2f));
                _animator.SetBool("IsDoubleJumping", true);
            }
            else if (_isWall && !_grounded)
            {
                if (jump)
                {
                    _animator.SetBool("IsJumping", true);
                    _animator.SetBool("JumpUp", true);
                    _rigidbody2D.velocity = new Vector2(0f, 0f);
                    _rigidbody2D.AddForce(new Vector2(transform.localScale.x * _jumpForce * 1.2f, _jumpForce));
                    _jumpWallStartX = transform.position.x;
                    _limitVelOnWallJump = true;
                    CanDoubleJump = true;
                    _canMove = false;
                }
                else if (dash && _canDash)
                {
                    CanDoubleJump = true;
                    StartCoroutine(DashCooldown());
                }
            }
        }
    }


    private void Flip()
    {
        _facingRight = !_facingRight;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        TextName.transform.Rotate(0f, 180f, 0f);
    }

    public void ApplyDamage(float damage, Vector3 position)
    {
        if (!Invincible)
        {
            //_animator.SetBool("Hit", true);
            Life -= damage;
            Vector2 damageDir = Vector3.Normalize(transform.position - position) * 40f;
            _rigidbody2D.velocity = Vector2.zero;
            _rigidbody2D.AddForce(damageDir * 10);
            if (Life <= 0)
            {
                StartCoroutine(WaitToDead());
            }
            else
            {
                StartCoroutine(Stun(0.25f));
                StartCoroutine(MakeInvincible(1f));
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Trap")
        {
            ApplyDamage(10, transform.position);
        }
    }

    public void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void NextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    IEnumerator DashCooldown()
    {
        _animator.SetBool("IsDashing", true);
        _isDashing = true;
        _canDash = false;
        yield return new WaitForSeconds(0.1f);
        _isDashing = false;
        yield return new WaitForSeconds(0.5f);
        _canDash = true;
    }

    IEnumerator Stun(float time)
    {
        _canMove = false;
        yield return new WaitForSeconds(time);
        _canMove = true;
    }
    IEnumerator MakeInvincible(float time)
    {
        Invincible = true;
        yield return new WaitForSeconds(time);
        Invincible = false;
    }
    IEnumerator WaitToMove(float time)
    {
        _canMove = false;
        yield return new WaitForSeconds(time);
        _canMove = true;
    }

    IEnumerator WaitToDead()
    {
        _animator.SetBool("IsDead", true);
        _canMove = false;
        Invincible = true;
        yield return new WaitForSeconds(0.4f);
        _rigidbody2D.velocity = new Vector2(0, _rigidbody2D.velocity.y);
        yield return new WaitForSeconds(1.1f);
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }
}

