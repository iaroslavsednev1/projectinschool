using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
using Photon.Pun;

public class HeroMovement : MonoBehaviour
{
    [SerializeField] private GameObject _console;
    [SerializeField] private CinemachineVirtualCamera _camera;
    private PhotonView _view;

    public Hero controller;
    public Animator animator;

    public float runSpeed = 40f;
    [SerializeField] private Text TextName;

    float horizontalMove = 0f;
    bool jump = false;
    bool dash = false;

    private void Start()
    {
        _view = GetComponent<PhotonView>();
        Cursor.visible = false;
        if (Multiplayer._isMultiplayer == true)
        {
            TextName.text = _view.Owner.NickName;
            if (!_view.IsMine)
            {
                _camera.enabled = false;
            }
        }
    }

    private void Update()
    {
        if(_view.IsMine || Multiplayer._isMultiplayer == true)
        {
            Control();
        }
        else if(Multiplayer._isMultiplayer == false)
        {
            Control();
        }
    }

    private void Control()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if (Input.GetKeyDown(KeyCode.Space))
        {
            jump = true;
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            dash = true;
        }

        if (Input.GetKeyDown(KeyCode.F1))
        {
            _console.SetActive(!_console.activeSelf);
        }
    }

    public void OnFall()
    {
        animator.SetBool("IsJumping", true);
    }

    public void OnLanding()
    {
        animator.SetBool("IsJumping", false);
    }

    private void FixedUpdate()
    {
        //if(_view.IsMine)
        //{
            controller.Move(horizontalMove * Time.fixedDeltaTime, jump, dash);
            jump = false;
            dash = false;
        //}
    }
}

