using UnityEngine;

public class World : SceneSwitch
{
    [SerializeField] private Transform _player;
    [SerializeField] private float _positionX;
    [SerializeField] private float _positionY;
    [SerializeField] private int _previous;

    public override void Start()
    {
        base.Start();

        if(PrevScene == _previous)
        {
            _player.position = new Vector2(_positionX, _positionY);
        }
    }
}
