using UnityEngine;
using UnityEngine.SceneManagement;

public class QuestionCategory : MonoBehaviour
{
    public void SelectCategory(string subject)
    {
        switch(subject)
        {
            case "Biology":
                SelectDataCategory(0);
                break;
            case "Geography":
                SelectDataCategory(1);
                break;
            case "Algebra":
                SelectDataCategory(2);
                break;
            case "Informatics":
                SelectDataCategory(3);
                break;
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private static void SelectDataCategory(int subjectIndex)
    {
        PlayerPrefs.SetInt("subject", subjectIndex);
    }
}
