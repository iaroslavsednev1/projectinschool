using UnityEngine;

public class Door : MonoBehaviour
{
    private bool _playerDetect;
    private SceneSwitch _sceneSwitch;

    [SerializeField] private GameObject _hint;
    [SerializeField] private int _sceneId;
    [SerializeField] private Transform _position;
    [SerializeField] private float _width;
    [SerializeField] private float _height;
    [SerializeField] private LayerMask _whatisPlayer;

    private void Start()
    {
        _sceneSwitch = FindObjectOfType<SceneSwitch>();
    }

    private void Update()
    {
        _playerDetect = Physics2D.OverlapBox(_position.position, new Vector2(_width, _height), 0, _whatisPlayer);

        if (_playerDetect == true)
        {
            _hint.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                _sceneSwitch.SwitchScene(_sceneId);
            }
        }

        if (_playerDetect == false)
        {
            _hint.SetActive(false);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(_position.position, new Vector3(_width, _height, 1));
    }
}