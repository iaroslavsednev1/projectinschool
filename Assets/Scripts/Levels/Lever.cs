using System.Collections;
using UnityEngine;

public class Lever : MonoBehaviour
{
    private Animator _animator;
    private bool _playerDetect;

    [SerializeField] private GameObject _hint;
    [SerializeField] private Transform _pos;
    [SerializeField] private float _width;
    [SerializeField] private float _height;
    [SerializeField] private LayerMask _whatisPlayer;
    [SerializeField] private int _leverIndex;
    [SerializeField] private GameObject _gameobjects;
    [SerializeField] private bool _inAnotherScene;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _animator.SetBool("IsLeverActive", false);
        //GameobjectState._activeGameobject = false;
    }

    private void Update()
    {
        _playerDetect = Physics2D.OverlapBox(_pos.position, new Vector2(_width, _height), 0, _whatisPlayer);

        if (_playerDetect == true)
        {
            _hint.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                _animator.SetBool("IsLeverActive", true);
                if(_inAnotherScene)
                {
                    GameobjectState._activeGameobject = true;
                }
                else
                {
                    _gameobjects.SetActive(true);
                }
                StartCoroutine(LeverActive());
            }
        }

        if (_playerDetect == false)
        {
            _hint.SetActive(false);
        }
    }


    IEnumerator LeverActive()
    {
        yield return new WaitForSeconds(0);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(_pos.position, new Vector3(_width, _height, 1));
    }
}

public static class GameobjectState
{
    public static bool _activeGameobject;
}
