using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneSwitchTrigger : MonoBehaviour
{
    private SceneSwitch _sceneSwitch;
    [SerializeField] private int _sceneId;

    private void Start()
    {
        _sceneSwitch = FindObjectOfType<SceneSwitch>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Hero>())
        {
            _sceneSwitch.SwitchScene(_sceneId);
        }
    }
}
