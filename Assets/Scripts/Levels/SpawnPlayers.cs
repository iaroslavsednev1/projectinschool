using UnityEngine;
using Photon.Pun;

public class SpawnPlayers : MonoBehaviour
{
    [SerializeField] private float positionX, positionY;
    public GameObject _player;

    private void Start()
    {
        Vector2 position = new(positionX, positionY);
        switch (Multiplayer._isMultiplayer)
        {
            case true:
                PhotonNetwork.Instantiate(_player.name, position, Quaternion.identity);
                break;
            case false:
                Instantiate(_player, position, Quaternion.identity);
                break;
        }
    }
}

public static class Multiplayer
{
    public static bool _isMultiplayer;
}
