using UnityEngine;
using UnityEngine.UI;

public class Star : MonoBehaviour
{
    [SerializeField] private Text _starCountText;
    private int _countStars;
    public int _countRightAnswer;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Hero>())
        {
            _countStars++;
            _starCountText.gameObject.SetActive(true);
            _starCountText.text = "���������: " + _countStars;
            Destroy(gameObject);
        }
    }
}
