using UnityEngine;

public class ChangeQuestions : MonoBehaviour
{
    [SerializeField] private Sprite[] _questions;
    [SerializeField] private Sprite[] _answers;
    [SerializeField] private GameObject[] _questionGameObjects;
    [SerializeField] private GameObject[] _answerGameObjects;

    public static Index[] Indices =
    {
        new Index(0, 3),
        new Index(3, 6),
        new Index(6, 9),
        new Index(9, 12),
    };

    private void Start()
    {
        int subject = PlayerPrefs.GetInt("subject");

        ChangeDataQuestion(Indices[subject]);
    }

    private void ChangeDataQuestion(Index index)
    {
        for (int i = index.FromIndex; i < index.ToIndex; i++)
        {
            _questionGameObjects[i].GetComponent<SpriteRenderer>().sprite = _questions[i];
            _answerGameObjects[i].GetComponent<SpriteRenderer>().sprite = _answers[i];
        }
    }
}

public class Index
{
    public int FromIndex;
    public int ToIndex;

    public Index(int fromindex, int toindex)
    {
        FromIndex = fromindex;
        ToIndex = toindex;
    }
}
