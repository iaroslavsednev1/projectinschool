using UnityEngine;

public class AnswerTube : MonoBehaviour
{
    private Hero _player;

    [SerializeField] private GameObject _hint;
    [SerializeField] private bool _isRightAnswer;
    [SerializeField] private Transform _pos;
    [SerializeField] private float _width;
    [SerializeField] private float _height;
    [SerializeField] private LayerMask _whatisPlayer;

    private bool _playerDetect;

    private void Update()
    {
        _playerDetect = Physics2D.OverlapBox(_pos.position, new Vector2(_width, _height), 0, _whatisPlayer);

        if (_playerDetect == true)
        {
            _hint.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                _player = FindObjectOfType<Hero>();
                if (_isRightAnswer)
                {
                    Answers._countRightAnswer++;
                }
                _player.GetComponent<Hero>().NextScene();
            }
        }

        if (_playerDetect == false)
        {
            _hint.SetActive(false);
        }
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(_pos.position, new Vector3(_width, _height, 1));
    }
}

public static class Answers
{
    public static int _countRightAnswer;
}
