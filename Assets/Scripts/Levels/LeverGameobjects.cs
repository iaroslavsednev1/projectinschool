using UnityEngine;

public class LeverGameobjects : MonoBehaviour
{
    public GameObject[] _gameobjects;
    [SerializeField] private int _gameobjectIndex;

    private void Update()
    {
        _gameobjects[_gameobjectIndex].SetActive(GameobjectState._activeGameobject);
    }
}
