using System.Collections;
using UnityEngine;

public class CycleDayNight : MonoBehaviour
{
    [SerializeField] private UnityEngine.Rendering.Universal.Light2D Sun;
    [SerializeField] private GameObject Background;
    [SerializeField] private Sprite NightBackground;

    private void Start()
    {
        StartCoroutine(SunSet());
    }
    
    IEnumerator SunSet()
    {
        while(Sun.intensity > 0.5f)
        {
            Sun.intensity -= 0.01f;
            yield return new WaitForSeconds(3f);
        }
        Background.GetComponent<SpriteRenderer>().sprite = NightBackground;
        StartCoroutine(SunRise());
        StopCoroutine(SunSet());
    }

    IEnumerator SunRise()
    {
        while (Sun.intensity < 1f)
        {
            Sun.intensity += 0.01f;
            yield return new WaitForSeconds(3f);
        }
        StartCoroutine(SunSet());
        StopCoroutine(SunRise());
    }
}