using System.Collections;
using UnityEngine;

public class Boat : MonoBehaviour
{
    private Animator _animator;
    private bool _playerDetect;
    private Hero _player;

    [SerializeField] private GameObject _hint;
    [SerializeField] private GameObject _boats;
    [SerializeField] private Transform _pos;
    [SerializeField] private float _width;
    [SerializeField] private float _height;
    [SerializeField] private LayerMask _whatisPlayer;

    private void Start()
    {
        _animator = GetComponentInParent<Animator>();
        _animator.SetBool("IsBoatMoving", false);
    }

    private void Update()
    {
        _playerDetect = Physics2D.OverlapBox(_pos.position, new Vector2(_width, _height), 0, _whatisPlayer);

        if (_playerDetect == true)
        {
            _hint.gameObject.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                StartCoroutine(SailBoat());
                _player = FindObjectOfType<Hero>();
                _player.GetComponent<SpriteRenderer>().enabled = false;
                _player.GetComponent<HeroMovement>().enabled = false;
                _hint.gameObject.SetActive(false);
                _animator.SetBool("IsBoatMoving", true);
            }
        }

        if (_playerDetect == false)
        {
            _hint.gameObject.SetActive(false);
        }
    }

    IEnumerator SailBoat()
    {
        yield return new WaitForSeconds(0.75f);
        _player.GetComponent<SpriteRenderer>().enabled = true;
        _player.GetComponent<HeroMovement>().enabled = true;
        _player.gameObject.transform.position = new Vector2(_boats.transform.position.x, _boats.transform.position.y);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(_pos.position, new Vector3(_width, _height, 1));
    }
}