using UnityEngine;
using IJunior.TypedScenes;
using UnityEngine.SceneManagement;

public class SceneSwitch : MonoBehaviour
{
    public static int PrevScene;
    public static int CurrentScene;

    public virtual void Start()
    {
        CurrentScene = SceneManager.GetActiveScene().buildIndex;
    }

    public void SwitchScene(int sceneId)
    {
        PrevScene = CurrentScene;
        SceneManager.LoadScene(sceneId);
    }
}
