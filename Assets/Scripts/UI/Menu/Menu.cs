using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    [SerializeField] private GameObject _selectSubjectPanel;
    [SerializeField] private GameObject _settingsPanel;

    public void CloseSelectSubjectPanel()
    {
        _selectSubjectPanel.SetActive(false);
    }

    public void StartGame()
    {
        _selectSubjectPanel.SetActive(true);
    }

    public void OpenSettings()
    {
        _settingsPanel.SetActive(true);
    }

    public void CloseSettings()
    {
        _settingsPanel.SetActive(false);
    }

    public void ConnectToTheServer()
    {
        SceneManager.LoadScene(46);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
