using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinPanel : MonoBehaviour
{
    [SerializeField] private Text _statistics;

    private void Start()
    {
        _statistics.text = "Правильно: " + Answers._countRightAnswer + "/3";
    }

    public void Continue()
    {
        SceneManager.LoadScene(0);
    }

}
