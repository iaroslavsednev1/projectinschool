using UnityEngine;

public class Training : MonoBehaviour
{
    public bool TrainingIsOver;
    [SerializeField] private DialogSystem dialogs;

    private void Start()
    {
        Cursor.visible = true;
        Time.timeScale = 0;
        TrainingIsOver = false;
    }

    public void ContinueButton()
    {
        gameObject.SetActive(false);
        Cursor.visible = false;
        Time.timeScale = 1;
        TrainingIsOver = true;
        dialogs.StartDialog();
    }
}
