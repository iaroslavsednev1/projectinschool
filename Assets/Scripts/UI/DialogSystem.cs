using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class DialogSystem : MonoBehaviour
{
    [SerializeField] private Text _text;
    [SerializeField] private Text _dialogSkipText;
    [SerializeField] private string[] _lines;
    [SerializeField] private float _textSpeed;

    private int index;

    private void Start()
    {
        _text.text = string.Empty;
        StartDialog();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (_text.text == _lines[index])
            {
                IsNextLine();
            }
            else
            {
                StopAllCoroutines();
                _text.text = _lines[index];
            }
        }

        if(Input.GetKeyDown(KeyCode.Return))
        {
            _text.gameObject.SetActive(false);
            _dialogSkipText.gameObject.SetActive(false);
        }
    }

    public void StartDialog()
    {
        index = 0;
        StartCoroutine(TypeLine());
    }

    IEnumerator TypeLine()
    {
        foreach (char c in _lines[index].ToCharArray())
        {

            _text.text += c;
            yield return new WaitForSeconds(_textSpeed);

        }
    }

    private void IsNextLine()
    {
        if (index < _lines.Length - 1)
        {
            index++;
            _text.text = string.Empty;
            StartCoroutine(TypeLine());
        }
        else
        {
            _text.gameObject.SetActive(false);
            _dialogSkipText.gameObject.SetActive(false);
        }
    }
}