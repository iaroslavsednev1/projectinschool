using UnityEngine;
using UnityEngine.SceneManagement;

public class PausePanel : MonoBehaviour
{
    [SerializeField] private GameObject _pausePanel;

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            _pausePanel.SetActive(true);
            Cursor.visible = true;
            Time.timeScale = 0;
        }
    }

    public void BackButton()
    {
        _pausePanel.SetActive(false);
        Cursor.visible = false;
        Time.timeScale = 1;
    }

    public void MenuButton()
    {
        SceneManager.LoadScene(0);
    }
}
