using UnityEngine;

public class ArrowTrap : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private Vector2 _moveVector;

    private Hero _player;
    private bool active = false;

    private void Start()
    {
        _player = FindObjectOfType<Hero>();
    }

    private void FixedUpdate()
    {
        if(active == true)
        {
            transform.Translate(_moveVector * _speed * Time.deltaTime);
            Destroy(this, 3);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Hero>())
        {
            active = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<Hero>())
        {
            Destroy(gameObject);
            _player.ApplyDamage(10, transform.position);
        }
    }
}