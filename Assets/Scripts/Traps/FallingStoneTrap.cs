using UnityEngine;

public class FallingStoneTrap : MonoBehaviour
{
    private Rigidbody2D _rigidbody;
    private Hero _player;

    private void Start()
    {
        _player = FindObjectOfType<Hero>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Hero>())
        {
            _rigidbody.isKinematic = false;
            Destroy(gameObject, 3f);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.GetComponent<Hero>())
        {
            _player.ApplyDamage(10, transform.position);
        }
    }
}
